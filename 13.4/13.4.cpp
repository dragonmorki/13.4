﻿#include "Helpers.h"
#include <iostream>

int main()
{
    int num1 = 5;
    int num2 = 10;

    int sum_sq = sum_of_squares(num1, num2);

    std::cout << sum_sq << std::endl;

    return 0;
}